// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './vuex/store';
import animated from 'animate.css'
import "swiper/dist/css/swiper.min.css";
import './assets/scss/index.scss';
import './assets/scss/element-variables.scss'
import { Input, Select, DatePicker, Message, Option, Form} from 'element-ui';
Vue.prototype.$ELEMENT = { size: 'small', zIndex: 3000 };
Vue.use(Input)
Vue.use(Select)
Vue.use(DatePicker)
Vue.use(Option)
Vue.use(Form)
Vue.prototype.$message = Message;
Vue.use(animated);
Vue.config.productionTip = false;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
