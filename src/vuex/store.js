import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex);
let store = new Vuex.Store({
  state: {
    _cityinfo: {
      name: '',
      id: 0,
      areaname: '全城',
      areaid: 0
    },
    productType: 0,
    caseType: 0
  },
  getters: {
    
  },
  mutations: {
    gotCityid(state, payload) {
      state._cityinfo = payload.data;
    }
  }
})

export default store