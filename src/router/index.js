/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-05-05 14:39:10
 * @LastEditTime: 2019-08-09 16:10:32
 * @LastEditors: Please set LastEditors
 */
import Vue from 'vue'
import Router from 'vue-router'
import store from '@/vuex/store';
Vue.use(Router);
let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: resolve => require(['../components/page/Home/index.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
      path: '/index',
      name: 'index',
      component: resolve => require(['../components/page/Home/index.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
      path: '/products',
      name: 'products',
      component: resolve => require(['../components/page/Wedding/product.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
      path: '/productsDetail',
      name: 'productsDetail',
      component: resolve => require(['../components/page/Wedding/productsDetail.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
      path: '/case',
      name: 'case',
      component: resolve => require(['../components/page/Wedding/case.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
      path: '/caseDetail',
      name: 'caseDetail',
      component: resolve => require(['../components/page/Wedding/caseDetail.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
      path: '/hotels',
      name: 'hotels',
      component: resolve => require(['../components/page/Hotel/index.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
       path: '/hoteldetail',
       name: 'hoteldetail',
       component: resolve => require(['../components/page/Hotel/detail.vue'], resolve),
       meta: {
         requiresCity: true
       }
    },
    {
       path: '/cars',
       name: 'cars',
       component: resolve => require(['../components/page/Car/index.vue'], resolve),
       meta: {
         requiresCity: true
       }
    },
    {
      path: '/cardetail',
      name: 'cardetail',
      component: resolve => require(['../components/page/Car/detail.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
      path: '/store',
      name: 'store',
      component: resolve => require(['../components/page/Door/index.vue'], resolve),
      meta: {
        requiresCity: true
      }
    },
    {
      path: '/join',
      name: 'join',
      component: resolve => require(['../components/page/Other/join.vue'], resolve)
    },
    {
      path: '/city',
      name: 'city',
      component: resolve => require(['../components/page/Home/city.vue'], resolve)
    },
    {
      path: '/privacy',
      name: 'privacy',
      component: resolve => require(['../components/page/Other/privacy.vue'], resolve)
    },
    {
      path: '/copyrights',
      name: 'copyrights',
      component: resolve => require(['../components/page/Other/copyrights.vue'], resolve)
    },
    {
      path: '/policy',
      name: 'policy',
      component: resolve => require(['../components/page/Other/policy.vue'], resolve)
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
router.beforeEach((to, from, next) => {

  if (sessionStorage.getItem('_cityinfo')) {
    store.commit({
      type: 'gotCityid',
      data: JSON.parse(sessionStorage.getItem('_cityinfo'))
    })
  }
  //页面
  if (to.matched.some(record => record.meta.requiresCity)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    let cityid = store.state._cityinfo.id
    if (cityid) {
      next()
    } else {
      next({
        path: '/city'
      })
    }
  } else {
    next() // 确保一定要调用 next()
  }
})
export default router
